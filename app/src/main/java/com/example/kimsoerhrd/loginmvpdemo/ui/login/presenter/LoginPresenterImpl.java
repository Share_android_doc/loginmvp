package com.example.kimsoerhrd.loginmvpdemo.ui.login.presenter;

import com.example.kimsoerhrd.loginmvpdemo.ui.login.interactor.Interactor;
import com.example.kimsoerhrd.loginmvpdemo.ui.login.interactor.LoginInteractorImpl;


public class LoginPresenterImpl implements Presenter.LoginPresenter, Interactor.LoginResponeInteractor {

    Presenter.LoginView view;

    Interactor interactor;

    public LoginPresenterImpl(Presenter.LoginView view) {
        this.view = view;
        interactor = new LoginInteractorImpl();
    }

    @Override
    public void onUserError() {
        if (view!=null){
            view.onUserNameError();
            view.hideProgressBar();
        }
    }

    @Override
    public void onPasswordError() {
            if (view!=null){
                view.onPasswordError();
                view.hideProgressBar();
            }
    }

    @Override
    public void onLoginSuccess() {
        if (view!=null){
            view.onLoginSuccess();
            view.hideProgressBar();
        }
    }

    @Override
    public void onDestroyView() {
        if (view!=null){

           view =null;
        }
    }

    @Override
    public void loginCredential(String username, String password) {
            if (view!=null){
                interactor.login(username, password, this);
                view.showProgressBar();
            }
    }
}
