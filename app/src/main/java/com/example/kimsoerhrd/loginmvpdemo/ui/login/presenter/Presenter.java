package com.example.kimsoerhrd.loginmvpdemo.ui.login.presenter;

public interface Presenter {

   public interface LoginView{
        void showProgressBar();
        void hideProgressBar();
        void onUserNameError();
        void onPasswordError();
        void onLoginSuccess();
    }

   public interface LoginPresenter{
        void onDestroyView();
        void loginCredential(String username, String password);
    }
}
