package com.example.kimsoerhrd.loginmvpdemo.ui.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.kimsoerhrd.loginmvpdemo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
