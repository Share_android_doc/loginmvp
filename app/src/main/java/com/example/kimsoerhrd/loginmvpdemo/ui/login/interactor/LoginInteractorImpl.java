package com.example.kimsoerhrd.loginmvpdemo.ui.login.interactor;

import android.text.TextUtils;

public class LoginInteractorImpl implements Interactor {

    @Override
    public void login(String username, String password, LoginResponeInteractor responeInteractor) {
        if (TextUtils.isEmpty(username)){
            responeInteractor.onUserError();
        }else if (TextUtils.isEmpty(password)){
            responeInteractor.onPasswordError();
        }else {
            responeInteractor.onLoginSuccess();
        }
    }
}
