package com.example.kimsoerhrd.loginmvpdemo.ui.login.view;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.kimsoerhrd.loginmvpdemo.R;
import com.example.kimsoerhrd.loginmvpdemo.base.BaseActivity;

import com.example.kimsoerhrd.loginmvpdemo.ui.login.presenter.LoginPresenterImpl;
import com.example.kimsoerhrd.loginmvpdemo.ui.login.presenter.Presenter;
import com.example.kimsoerhrd.loginmvpdemo.ui.main.MainActivity;

public class LoginActivity extends BaseActivity implements Presenter.LoginView {
    EditText username, password;
    ProgressBar progressBar;

    Presenter.LoginPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.etUsername);
        password = findViewById(R.id.etPassword);
        progressBar = findViewById(R.id.progressBar);
        presenter = new LoginPresenterImpl(this);

    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
    }

    @Override
    public void onUserNameError() {
        username.setError("Error User name");
    }

    @Override
    public void onPasswordError() {
        password.setError("Error Password");
    }

    @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroyView();
    }

    public void onLogin(View view) {
        presenter.loginCredential(username.getText().toString(), password.getText().toString());

    }
}
