package com.example.kimsoerhrd.loginmvpdemo.ui.login.interactor;

public interface Interactor {

    void login(String username, String password, LoginResponeInteractor responeInteractor);
    interface LoginResponeInteractor{
        void onUserError();
        void onPasswordError();
        void onLoginSuccess();
    }
}
